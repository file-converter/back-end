import { expect } from "chai";
import { Server } from "./../../../src/server";
import { FormatCtrl } from "../../../src/controllers";
import { inject, bootstrap } from "@tsed/testing";
import { ExpressApplication } from "@tsed/common";
import supertest = require("supertest");

const FORMATS = require('./../../../resources/formats.json');

describe('FormatCtrl', () => {
	beforeEach(bootstrap(Server));

	it('should be an instance of', inject([FormatCtrl], async (instance: FormatCtrl) => {
		expect(instance).to.be.an.instanceof(FormatCtrl);
	}));

	it('should get all available formats via method', inject([FormatCtrl], async (instance: FormatCtrl) => {
		const formats = await instance.findAll();
		expect(formats).to.deep.equals(FORMATS);
	}));

	it('should get all available formats via REST API', inject([ExpressApplication], (expressApplication: ExpressApplication) => {
		supertest(expressApplication)
			.get('/api/formats')
			.expect(200)
			.end((err, response) => {
				if (err) {
					throw err;
				}
				expect(response.body).to.deep.equals(FORMATS);
			});
	}));
});
