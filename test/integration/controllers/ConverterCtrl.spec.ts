import { ConverterService } from "./../../../src/services/ConverterService";
import { expect } from "chai";
import { ConverterCtrl } from "./../../../src/controllers/ConverterCtrl";
import { Server } from "./../../../src/server";
import { bootstrap, inject } from "@tsed/testing";
import { TestFiles } from './../../types';
import { ControllerService } from "@tsed/common";

describe('ConverterCtrl', () => {
	beforeEach(bootstrap(Server));

	// TODO: write tests to test endpoints

	// NOTE: TSED dependency injection doesn't work correctly, need to find a workaround
});
