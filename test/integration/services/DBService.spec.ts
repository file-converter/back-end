import { DBService } from "../../../src/services";
import { inject } from '@tsed/testing';
import { expect } from 'chai';

interface DBEntry {
	current_database: string;
}

describe('DBService', () => {
	let dbService: DBService;

	before(() => {
		dbService = new DBService();
	});

	it('should be an instance of', () => {
		expect(dbService).to.be.an.instanceof(DBService);
	});

	it('should execute a query successfully', () => {
		dbService.executeQuery('SELECT current_database()').then((res: DBEntry[]) => {
			expect(res[0].current_database).equals('fileconverterdb');
		});
	});
});
