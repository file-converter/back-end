import { FormatModel } from "./../../../src/models/FormatModel";
import { expect } from "chai";
import { FormatService, DBService } from "../../../src/services";

describe('FormatService', () => {
	let formatService: FormatService;
	const FORMATS: FormatModel[] = require('./../../../resources/formats.json');

	before(() => {
		formatService = new FormatService(new DBService());
	});

	it('should be an instance of', () => {
		expect(formatService).to.be.an.instanceof(FormatService);
	});

	it('should return all available formats', async () => {
		expect(formatService.findAll()).to.be.an.instanceof(Promise);
		const formats = await formatService.findAll();
		expect(formats).to.deep.equals(FORMATS);
	});
});
