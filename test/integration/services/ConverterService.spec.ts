import { PROJECT_DIR, FILE_DIR } from "./../../../src/server";
import { expect } from "chai";
import { ConverterService } from "./../../../src/services/ConverterService";
import { DBService } from "../../../src/services";
import { TestFiles } from './../../types';
import * as fs from 'fs';

describe('ConverterService', () => {
	let converterService: ConverterService;
	const RESOURCE_DIR = `${PROJECT_DIR}resources`;

	const TEST_FILES: TestFiles = {
		image: {
			fieldname: 'image',
			originalname: 'backend.jpg',
			encoding: '7bit',
			mimetype: 'image/jpeg',
			destination: '/home/ivan/mdrive/fc-files',
			filename: '1577cb59da1cbb7b1235adcb72a30212',
			path: '/home/ivan/mdrive/fc-files/1577cb59da1cbb7b1235adcb72a30212',
			size: 2856407,
			buffer: null
		},
		video: {
			fieldname: 'video',
			originalname: 'jazz-dancing-2.webm',
			encoding: '7bit',
			mimetype: 'video/webm',
			destination: '/home/ivan/mdrive/fc-files',
			filename: 'c0709e95a6c0bb61bee762e4a1378d11',
			path: '/home/ivan/mdrive/fc-files/c0709e95a6c0bb61bee762e4a1378d11',
			size: 3366629,
			buffer: null
		}
	};
	let imageUUID: string;
	let videoUUID: string;

	before(() => {
		converterService = new ConverterService(new DBService());
	});

	after(() => {
		// reset test files
		fs.copyFileSync(`${FILE_DIR}/${imageUUID}/backend.jpg`, `${RESOURCE_DIR}/backend.jpg`);
		fs.copyFileSync(`${FILE_DIR}/${videoUUID}/jazz-dancing-2.webm`, `${RESOURCE_DIR}/jazz-dancing-2.webm`);
	});

	it('should be an instance of', () => {
		expect(converterService).to.be.an.instanceof(ConverterService);
	});

	describe('should handle a conversion request for image (jpg => png)', () => {
		it('should create a conversion entry', async () => {
			imageUUID = await converterService.createConversionEntry('png', TEST_FILES.image);
			expect(imageUUID.length).to.equals(36);
		});

		it('should create a directory', () => {
			expect(fs.existsSync(`${FILE_DIR}/${imageUUID}`)).to.be.false;
			converterService.createFileDirectory(FILE_DIR, imageUUID);
			expect(fs.existsSync(`${FILE_DIR}/${imageUUID}`)).to.be.true;
		});

		it('should move a file', () => {
			// simulated data for testing purposes
			const NEW_PATH = `${FILE_DIR}/${imageUUID}/${TEST_FILES.image.originalname}`;
			const CURRENT_PATH = `${RESOURCE_DIR}/backend.jpg`;
			TEST_FILES.image.path = CURRENT_PATH;

			expect(fs.existsSync(NEW_PATH)).to.be.false;
			converterService.moveFile(TEST_FILES.image, imageUUID);
			expect(fs.existsSync(NEW_PATH)).to.be.true;
		});

		it('should convert an image file', async () => {
			// simulated data for testing purposes
			TEST_FILES.image.path = `${FILE_DIR}/${imageUUID}/${TEST_FILES.image.originalname}`;
			TEST_FILES.image.destination = `${FILE_DIR}/${imageUUID}`;
			const NEW_FILE = `${TEST_FILES.image.destination}/backend.png`;

			expect(fs.existsSync(NEW_FILE)).to.be.false;
			converterService.convertFile('png', TEST_FILES.image).then(() => {
				expect(fs.existsSync(NEW_FILE)).to.be.true;
			});
		});
	});

	describe('should handle a conversion request for video (webm => mp4)', () => {
		it('should create a conversion entry', async () => {
			videoUUID = await converterService.createConversionEntry('mp4', TEST_FILES.video);
			expect(videoUUID.length).to.be.equals(36);
		});

		it('should create a directory', () => {
			expect(fs.existsSync(`${FILE_DIR}/${videoUUID}`)).to.be.false;
			converterService.createFileDirectory(FILE_DIR, videoUUID);
			expect(fs.existsSync(`${FILE_DIR}/${videoUUID}`)).to.be.true;
		});

		it('should move a file', () => {
			// simulated data for testing purposes
			const NEW_PATH = `${FILE_DIR}/${videoUUID}/${TEST_FILES.video.originalname}`;
			const CURRENT_PATH = `${RESOURCE_DIR}/jazz-dancing-2.webm`;
			TEST_FILES.video.path = CURRENT_PATH;

			expect(fs.existsSync(NEW_PATH)).to.be.false;
			converterService.moveFile(TEST_FILES.video, videoUUID);
			expect(fs.existsSync(NEW_PATH)).to.be.true;
		});

		it('should convert an video file', async () => {
			// simulated data for testing purposes
			TEST_FILES.video.path = `${FILE_DIR}/${videoUUID}/${TEST_FILES.video.originalname}`;
			TEST_FILES.video.destination = `${FILE_DIR}/${videoUUID}`;
			const NEW_FILE = `${TEST_FILES.video.destination}/jazz-dancing-2.mp4`;

			expect(fs.existsSync(NEW_FILE)).to.be.false;
			converterService.convertFile('mp4', TEST_FILES.video).then(() => {
				expect(fs.existsSync(NEW_FILE)).to.be.true;
			});
		});
	});

	describe('should handle file finding requests', async () => {
		it('should get file data', () => {
			converterService.getFile(imageUUID).then(image => {
				expect(image).to.not.be.empty;
			});
		});
	});
});
