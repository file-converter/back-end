# BackEnd

This project is provides a RESTful API for converting images and videos

NOTE: in order to use this server a corresponding PostgreSQL DB needs to be live

## Running server

Run `npm run start` to start the server

![](./.local/running.gif)

## Running unit tests

Run `npm run test:unit` to execute the unit tests

![](./.local/testing.gif)
