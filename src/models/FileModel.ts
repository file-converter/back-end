import { Required, Format, Default } from "@tsed/common";

export class FileModel {
	id: string;
	@Required() filename: string;
	@Required() old_format: string;
	@Required() new_format: string;
	@Required() directory: string;
}
