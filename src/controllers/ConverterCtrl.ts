import * as LocalConverter from "./../services/ConverterService";
import { Controller, PathParams, Post, Get, HeaderParams, Res, Req } from "@tsed/common";
import { MultipartFile } from '@tsed/multipartfiles';
import * as fs from 'fs';

@Controller('/converter')
export class ConverterCtrl {
	constructor(private converterService: LocalConverter.ConverterService) { }

	/**
	 * Searches for a converted file through the database, finds it in the local
	 * filesystem, and returns it
	 * @param id UUID that has been assigned to the file
	 * @param response to configure sending file
	 * @returns {void} (the file is piped through the response)
	 */
	@Get('/file')
	findFile(
		@HeaderParams('uuid') id: string,
		@Res() response: Res,
	) {
		this.converterService.handleFindFileRequest(id, response);
	}

	/**
	 * Converts a file to a new format
	 * @param newFormat format to be converted to
	 * @param file information when receiving from HTTP
	 * @returns {Promise<string>}
	 */
	@Post('/:new')
	async convert(
		@PathParams('new') newFormat: string,
		@MultipartFile() file: Express.Multer.File
	): Promise<string> {
		return new Promise(resolve => {
			this.converterService.handleConversionRequest(newFormat, file).then(resolve);
		});
	}
}
