import { Controller, Get } from "@tsed/common";
import { FormatService } from "../services";
import { FormatModel } from "src/models/FormatModel";

@Controller('/formats')
export class FormatCtrl {
	constructor(private formatService: FormatService) { }

	/**
	 * Finds all available formats
	 * @returns {FormatModel[]}
	 */
	@Get('/')
	findAll(): Promise<FormatModel[]> {
		return new Promise(resolve => {
			this.formatService.findAll().then(resolve);
		});
	}
}
