import { GlobalAcceptMimesMiddleware, ServerLoader, ServerSettings, $log } from '@tsed/common';
import * as AllCtrl from './controllers/index';
import * as AllServices from './services/index';
import { Pool } from 'pg';
import * as fs from 'fs';
import * as cors from 'cors';
import * as os from 'os';

export const GLOBAL_DB_POOL = new Pool({
	user: 'postgres',
	host: 'localhost',
	database: 'fileconverterdb',
	password: 'qwerty',
	port: 5432
});
export const PROJECT_DIR = `${__dirname}/../`;
export const FILE_DIR = `${os.userInfo().homedir}/mdrive/fc-files`;

@ServerSettings({
	__dirname,
	acceptMimes: [
		'application/json',
		'multipart/form-data',
		'image/*'
	],
	mount: {
		"/api/": Object.values(AllCtrl)
	},
	componentsScan: Object.keys(AllServices),
	uploadDir: FILE_DIR
})
export class Server extends ServerLoader {
	/**
	 * This method lets you configure the express middleware required by your application to work.
	 * @returns {Server}
	 */
	public $beforeRoutesInit(): void | Promise<any> {
		this
			.use(GlobalAcceptMimesMiddleware)
			.use(cors());
		this.handleFileDirectory();
	}

	private handleFileDirectory() {
		if (!fs.existsSync(FILE_DIR)) {
			fs.mkdirSync(FILE_DIR);
		}
	}
}
