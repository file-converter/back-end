import { Service, Scope, ProviderScope } from "@tsed/di";
import { Pool } from "pg";
import { GLOBAL_DB_POOL } from "../server";

@Service()
export class DBService {
	private pool: Pool;

	constructor() {
		this.pool = GLOBAL_DB_POOL;
	}

	/**
	 * Executes any PostgreSQL query
	 * @param query - example: 'SELECT * FROM formats'
	 * @returns {any[]}
	 */
	executeQuery(query) {
		return new Promise(resolve => {
			this.pool.query(query, (err, results) => {
				if (err) {
					throw err;
				}
				resolve(results.rows);
			});
		});
	}
}
