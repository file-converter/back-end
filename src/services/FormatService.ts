import { Service } from '@tsed/di';
import { DBService } from './DBService';
import { FormatModel } from 'src/models/FormatModel';

@Service()
export class FormatService {
	constructor(private dbService: DBService) { }

	/**
	 * Finds all formats available for conversion
	 * @returns {FormatModel[]}
	 */
	findAll(): Promise<FormatModel[]> {
		return new Promise(resolve => {
			this.dbService.executeQuery('SELECT * FROM formats').then(resolve);
		});
	}
}
