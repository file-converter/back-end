import { FileModel } from "./../models/FileModel";
import { Service } from "@tsed/di";
import { DBService } from "./DBService";
import * as fs from 'fs';
import * as ffmpeg from 'fluent-ffmpeg';
import Jimp from 'jimp/es';
import { $log, Res } from "@tsed/common";


@Service()
export class ConverterService {
	constructor(private dbService: DBService) { }

	/**
	 * Handles the request for file conversion
	 * @param newFormat format to be converted to
	 * @param file information when receiving from HTTP
	 * @returns {Promise<string>}
	 */
	handleConversionRequest(
		newFormat: string,
		file: Express.Multer.File
	): Promise<string> {
		return new Promise(resolve => {
			this.createConversionEntry(newFormat, file).then((id: string) => {
				this.createFileDirectory(file.destination, id);
				this.moveFile(file, id);
				this.convertFile(newFormat, file).then(() => resolve(id));
			});
		});
	}

	/**
	 * Handles the request for finding a converted file
	 * @param id UUID that has been assigned to the file
	 * @param response to configure sending file
	 * @returns {void} (the file is piped through the response)
	 */
	handleFindFileRequest(id: string, response: Res) {
		response.writeHead(200, { 'Content-disposition': 'attachment' });
		this.getFile(id).then(file => {
			const fileLocation = `${file.directory}/${id}/${file.filename}.${file.new_format}`;
			const fileStream = fs.createReadStream(fileLocation);
			fileStream.pipe(response);
		});
	}

	/**
	 * Creates a conversion entry in the database
	 * @param newFormat format to be converted to
	 * @param file information when receiving from HTTP
	 * @returns {Promise<string>}
	 */
	createConversionEntry(newFormat: string, file: Express.Multer.File): Promise<string> {
		return new Promise(resolve => {
			const filenameInfo = file.originalname.split('.');
			this.dbService.executeQuery(`
				INSERT INTO conversions (filename, old_format, new_format, directory)
				VALUES
				(
					'${filenameInfo[0]}',
					'${filenameInfo[1]}',
					'${newFormat}',
					'${file.destination}'
				)
				RETURNING id
			`).then((entry: FileModel[]) => {
				resolve(entry[0].id);
			});
		});
	}

	/**
	 * Creates a new directory with the UUID of the file
	 * @param destination where a new directory needs to be made
	 * @param id UUID that has been assigned to the file
	 */
	createFileDirectory(destination: string, id: string) {
		fs.mkdirSync(`${destination}/${id}`);
	}

	/**
	 * When uploaded a file is automatically moved to the FILE_DIR (server.ts)
	 * This method moves the original file to the corresponding directory with its converted counterpart
	 * @param file information when receiving from HTTP
	 * @param id UUID that has been assigned to the file
	 */
	moveFile(file: Express.Multer.File, id: string) {
		const newPath = `${file.destination}/${id}/${file.originalname}`;
		fs.renameSync(file.path, newPath);
		file.destination = `${file.destination}/${id}`;
		file.path = newPath;
	}

	/**
	 * Converts a file to the new format
	 * @param newFormat format to be converted to
	 * @param file information when receiving from HTTP
	 * @returns {Promise}
	 */
	convertFile(newFormat: string, file: Express.Multer.File) {
		return new Promise(resolve => {
			const newPath = `${file.destination}/${file.originalname.split('.')[0]}.${newFormat}`;
			switch (file.mimetype.split('/')[0]) {
				case 'image':
					Jimp.read(file.path).then(image => {
						image.write(newPath, () => {
							resolve();
						});
					});
					break;
				case 'video':
					const videoPath = `${file.destination}/${file.originalname}`;
					ffmpeg(videoPath)
						.toFormat(newFormat)
						.on('end', resolve)
						.saveToFile(newPath);
					break;
			}
		});
	}

	/**
	 * Gets the entry of a file from the database
	 * @param id UUID that has been assigned to the file
	 * @returns {Promise<FileModel>}
	 */
	getFile(id: string): Promise<FileModel> {
		return new Promise(resolve => {
			this.dbService.executeQuery(`
				SELECT *
				FROM conversions
				WHERE id = '${id}'
			`).then(entry => {
				resolve(entry[0]);
			});
		});
	}
}
